﻿CREATE TABLE [dbo].[felhasznalo] (
    [nev]             VARCHAR (50)  NOT NULL,
    [felhasznalonev]  VARCHAR (50)  NULL,
    [email]           VARCHAR (50)  NOT NULL,
    [jelszo]          VARCHAR (300) NULL,
    [kupon]           NUMERIC (3)   NULL,
    [adminisztratore] NUMERIC (1)   NULL,
    CONSTRAINT [felhasznalo_pk] PRIMARY KEY CLUSTERED ([nev] ASC, [email] ASC),
    CONSTRAINT [felhasznalo_ck] CHECK ([adminisztratore] >= (0)
                                           AND [adminisztratore] <= (1))
);
CREATE TABLE [dbo].[konyvek] (
    [mufaj]         VARCHAR (50) NULL,
    [ar]            NUMERIC (10) NOT NULL,
    [szerzo]        VARCHAR (50) NULL,
    [ISBN]          VARCHAR (20) NOT NULL,
    [cim]           VARCHAR (50) NULL,
    [ev]            NUMERIC (4)  NULL,
    [engedelyezvee] NUMERIC (1)  NULL,
    CONSTRAINT [konyvek_pk] PRIMARY KEY CLUSTERED ([ISBN] ASC),
    CONSTRAINT [konyvek_ck] CHECK ([engedelyezvee]>=(0) AND [engedelyezvee]<=(1))
);

CREATE TABLE [dbo].[publikal] (
    [publikalas_id] NUMERIC (4)  NOT NULL,
    [ISBN]          VARCHAR (20) NULL,
    [nev]           VARCHAR (50) NULL,
    [email]         VARCHAR (50) NULL,
    CONSTRAINT [publikalas_pk] PRIMARY KEY CLUSTERED ([publikalas_id] ASC),
    CONSTRAINT [publikalo_fk] FOREIGN KEY ([nev], [email]) REFERENCES [dbo].[felhasznalo] ([nev], [email]),
    CONSTRAINT [publikalt_fk] FOREIGN KEY ([ISBN]) REFERENCES [dbo].[konyvek] ([ISBN])
);

CREATE TABLE [dbo].[vasarol] (
    [vasarlas_id] NUMERIC (4)  NOT NULL,
    [ISBN]        VARCHAR (20) NULL,
    [nev]         VARCHAR (50) NULL,
    [email]       VARCHAR (50) NULL,
    CONSTRAINT [vasarlas_pk] PRIMARY KEY CLUSTERED ([vasarlas_id] ASC),
    CONSTRAINT [vasarlo_fk] FOREIGN KEY ([nev], [email]) REFERENCES [dbo].[felhasznalo] ([nev], [email]),
    CONSTRAINT [vasarolt_fk] FOREIGN KEY ([ISBN]) REFERENCES [dbo].[konyvek] ([ISBN])
);

INSERT INTO [dbo].[felhasznalo] ([nev], [felhasznalonev], [email], [jelszo], [kupon], [adminisztratore]) VALUES (N'Dr. Lennart Hofstatter', N'len999', N'drLenH@bigbang.com', N'58a949dd2f91a14b575685b217d2eada8a1a1d1b53d342cc302ceb7c6eaeb6eb', CAST(0 AS Decimal(3, 0)), CAST(0 AS Decimal(1, 0)))
INSERT INTO [dbo].[felhasznalo] ([nev], [felhasznalonev], [email], [jelszo], [kupon], [adminisztratore]) VALUES (N'Dr. Rajesh Koothrapali', N'raj999', N'drRajK@bigbang.com', N'980397771f533e7b6cfb63f293e85da4720ee9a828e7acf3e0338296988c6558', CAST(10 AS Decimal(3, 0)), CAST(0 AS Decimal(1, 0)))
INSERT INTO [dbo].[felhasznalo] ([nev], [felhasznalonev], [email], [jelszo], [kupon], [adminisztratore]) VALUES (N'Dr. Sheldon Cooper', N'she999', N'drSheC@bigbang.com', N'c074e551f7132ae78a1fc4a31c1addce65faa38dab9c315efd6b917ce3662663', CAST(0 AS Decimal(3, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[felhasznalo] ([nev], [felhasznalonev], [email], [jelszo], [kupon], [adminisztratore]) VALUES (N'Mr. Howard Wolowitz', N'how999', N'mrHowW@bigbang.com', N'3a0e55789fefb7cbf80f3c4c029fe299cd9df2d5d2e9087c9887227c15843ad2', CAST(15 AS Decimal(3, 0)), CAST(0 AS Decimal(1, 0)))

INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'fantasy', CAST(3000 AS Decimal(10, 0)), N'George R. R. Martin', N'111-111-111-1111', N'Trónok Harca', CAST(1997 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'fantasy', CAST(3125 AS Decimal(10, 0)), N'George R. R. Martin', N'111-111-111-1112', N'Királyok csatája', CAST(1998 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'fantasy', CAST(3125 AS Decimal(10, 0)), N'George R. R. Martin', N'111-111-111-1113', N'Kardok vihara', CAST(2000 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'fantasy', CAST(3125 AS Decimal(10, 0)), N'George R. R. Martin', N'111-111-111-1114', N'Varjak lakomája', CAST(2005 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'fantasy', CAST(3125 AS Decimal(10, 0)), N'George R. R. Martin', N'111-111-111-1115', N'Sárkányok tánca', CAST(2011 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'szakirodalom', CAST(2000 AS Decimal(10, 0)), N'Dr. Lennart Hofstatter', N'111-111-111-2222', N'Húrelmélet', CAST(2001 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'szakirodalom', CAST(1500 AS Decimal(10, 0)), N'Stephen Hawking', N'111-111-111-2223', N'Az ido rövid története', CAST(1988 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'szépirodalom', CAST(2500 AS Decimal(10, 0)), N'Rejto Jeno', N'111-111-111-3331', N'Piszkos Fred a kapitány', CAST(1950 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'szépirodalom', CAST(2212 AS Decimal(10, 0)), N'Rejto Jeno', N'111-111-111-3332', N'Vesztegzár a Grand Hotelban', CAST(1955 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'szépirodalom', CAST(1985 AS Decimal(10, 0)), N'Rejto Jeno', N'111-111-111-3333', N'Három testor Afrikában', CAST(1945 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'sport', CAST(2006 AS Decimal(10, 0)), N'Football Ferenc', N'111-111-111-4441', N'6-3', CAST(2000 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))
INSERT INTO [dbo].[konyvek] ([mufaj], [ar], [szerzo], [ISBN], [cim], [ev], [engedelyezvee]) VALUES (N'sport', CAST(2006 AS Decimal(10, 0)), N'Kosár Károly', N'111-111-111-4442', N'Cleveland Cavaliers', CAST(2004 AS Decimal(4, 0)), CAST(1 AS Decimal(1, 0)))

INSERT INTO [dbo].[publikal] ([publikalas_id], [ISBN], [nev], [email]) VALUES (CAST(1 AS Decimal(4, 0)), N'111-111-111-1111', N'Mr. Howard Wolowitz', N'mrHowW@bigbang.com')

INSERT INTO [dbo].[vasarol] ([vasarlas_id], [ISBN], [nev], [email]) VALUES (CAST(1 AS Decimal(4, 0)), N'111-111-111-2222', N'Dr. Rajesh Koothrapali', N'drRajK@bigbang.com')

);

CREATE table publikal (
publikalas_id number(4),
ISBN varchar(20),
nev varchar(50),
email varchar(50),
constraint publikalas_pk
primary key(publikalas_id),
constraint publikalo_fk
foreign key(nev, email)
references felhasznalo(nev, email),
constraint publikalt_fk
foreign key(ISBN)
references konyvek(ISBN)
);



INSERT into felhasznalo (nev, felhasznalonev, email, jelszo, kupon, adminisztratore) values('Dr. Rajesh Koothrapali', 'raj999', 'drRajK@bigbang.com', 'asd000', 10, 0);
INSERT into felhasznalo (nev, felhasznalonev, email, jelszo, kupon, adminisztratore) values('Dr. Sheldon Cooper', 'she999', 'drSheC@bigbang.com', 'asd001', 0, 1);
INSERT into felhasznalo (nev, felhasznalonev, email, jelszo, kupon, adminisztratore) values('Dr. Lennart Hofstatter', 'len999', 'drLenH@bigbang.com', 'asd002', 0, 0);
INSERT into felhasznalo (nev, felhasznalonev, email, jelszo, kupon, adminisztratore) values('Mr. Howard Wolowitz', 'how999', 'mrHowW@bigbang.com', 'asd003', 15, 0);

INSERT into konyvek (mufaj, ar, szerzo, ISBN, cim, ev, engedelyezvee) values('fantasy', 3000, 'George R. R. Martin' , '111-111-111-1111', 'Trónok Harca', 1997, 0);
INSERT into konyvek (mufaj, ar, szerzo, ISBN, cim, ev, engedelyezvee) values('szakirodalom', 2000, 'Dr. Lennart Hofstatter' , '111-111-111-2222', 'Húrelmélet', 2001, 1);

INSERT into vasarol (vasarlas_id, ISBN, nev, email) values(1, '111-111-111-2222', 'Dr. Rajesh Koothrapali', 'drRajK@bigbang.com');

INSERT into publikal (publikalas_id, ISBN, nev, email) values(1, '111-111-111-1111', 'Mr. Howard Wolowitz', 'mrHowW@bigbang.com');

