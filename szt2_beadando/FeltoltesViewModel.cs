﻿// <copyright file="FeltoltesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Feltöltés ablak ViewModelje
    /// </summary>
    public class FeltoltesViewModel
    {
        private FeltoltoFelulet bL;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeltoltesViewModel"/> class.
        /// </summary>
        public FeltoltesViewModel()
        {
            this.bL = new FeltoltoFelulet();
        }

        /// <summary>
        /// Ellenőrzi, hogy sikeres-e a feltöltés.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        /// <param name="szerzo">A könyv szerző paramétere.</param>
        /// <param name="cim">A könyv cím paramétere.</param>
        /// <param name="mufaj">A könyv műfaj paramétere.</param>
        /// <param name="ev">A könyv év paramétere.</param>
        /// <param name="ar">A könyv ár paramétere.</param>
        /// <returns>Igaz ha sikeres a feltöltés.</returns>
        public bool SikeresFeltoltes(string isbn, string szerzo, string cim, string mufaj, int ev, int ar)
        {
            if (this.bL.FeltoltesSikeres(isbn, szerzo, cim, mufaj, ev, ar) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Publikáláshoz hozzáadja a könyvet.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        public void PublikalhozHozzaad(string isbn)
        {
            this.bL.KonyvetHozzaad(isbn);
        }
    }
}
