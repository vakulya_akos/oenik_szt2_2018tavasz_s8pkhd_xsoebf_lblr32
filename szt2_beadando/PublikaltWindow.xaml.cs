﻿// <copyright file="PublikaltWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;

    /// <summary>
    /// Interaction logic for PublikaltWindow.xaml
    /// </summary>
    public partial class PublikaltWindow : Window
    {
        private PublikaltWindowViewModel pwvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublikaltWindow"/> class.
        /// </summary>
        public PublikaltWindow()
        {
            this.InitializeComponent();
            this.pwvm = new PublikaltWindowViewModel();
            this.DataContext = this.pwvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
