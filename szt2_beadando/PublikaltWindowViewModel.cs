﻿// <copyright file="PublikaltWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    /// <summary>
    /// Publikálás ViewModelje.
    /// </summary>
    public class PublikaltWindowViewModel
    {
        private List<Data.publikal> publikalasaim;

        private List<Data.vasarol> vasarlasaim;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublikaltWindowViewModel"/> class.
        /// </summary>
        public PublikaltWindowViewModel()
        {
            this.vasarlasaim = Logic.WeBestSellerBL.AktualisFelhVasarlasai();
            this.publikalasaim = Logic.WeBestSellerBL.AktualisFelhPublikálásai();
        }

        /// <summary>
        /// Gets or sets publikalasaim.
        /// </summary>
        public List<publikal> Publikalasaim
        {
            get
            {
                return this.publikalasaim;
            }

            set
            {
                this.publikalasaim = value;
            }
        }

        /// <summary>
        /// Gets or sets vasarlasaim.
        /// </summary>
        public List<vasarol> Vasarlasaim
        {
            get
            {
                return this.vasarlasaim;
            }

            set
            {
                this.vasarlasaim = value;
            }
        }
    }
}
