﻿// <copyright file="FizetesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using Logic;

    /// <summary>
    /// Fizetés ablak ViewModelje.
    /// </summary>
    public class FizetesViewModel
    {
        private WeBestSellerBL bL;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizetesViewModel"/> class.
        /// </summary>
        public FizetesViewModel()
        {
            this.bL = new WeBestSellerBL();
        }

        /// <summary>
        /// A felhasználó vásárlásaihoz hozzáadja.
        /// </summary>
        public void VasarlashozHozzaAd()
        {
            this.bL.VasarlashozHozzaAd();
        }
    }
}
