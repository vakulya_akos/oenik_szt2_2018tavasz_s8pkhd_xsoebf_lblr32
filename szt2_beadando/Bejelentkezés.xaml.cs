﻿// <copyright file="Bejelentkezés.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;
    using Logic;

    /// <summary>
    /// Interaction logic for Bejelentkezés.xaml
    /// </summary>
    public partial class Bejelentkezés : Window
    {
        private BejelentkezesViewModel bVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bejelentkezés"/> class.
        /// </summary>
        public Bejelentkezés()
        {
            this.InitializeComponent();
            this.BVM = new BejelentkezesViewModel();
        }

        /// <summary>
        /// Gets or sets BVM
        /// </summary>
        public BejelentkezesViewModel BVM
        {
            get
            {
                return this.bVM;
            }

            set
            {
                this.bVM = value;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.adminRadio.IsChecked == true || this.FelhasznaloRadio.IsChecked == true)
            {
                // nincs paraméter
                if (this.BVM.SikeresBejelentkezes(this.tb_felhasznalonev.Text, this.pb_jelszo.Password.ToString()) == true)
                {
                    WeBestSellerBL.Felhasznalo = this.tb_felhasznalonev.Text;
                    if (this.BVM.Adminkent() && this.adminRadio.IsChecked == true)
                    {
                        Adminisztrálás a = new Adminisztrálás();
                        this.Close();
                        a.ShowDialog();
                    }
                    else
                    if (!this.BVM.Adminkent() && this.adminRadio.IsChecked == true)
                    {
                        MessageBox.Show("Nincs jogosultságod az admin ablakhoz!");
                    }
                    else
                    {
                        Böngészés b = new Böngészés();
                        this.Close();
                        b.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Sikertelen bejelentkezés (Valamely adatod hibás vagy nem adtad meg!)");
                }
            }
            else
            {
                MessageBox.Show("Jelöld be a szerepkört!");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
