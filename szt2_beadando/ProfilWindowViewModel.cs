﻿// <copyright file="ProfilWindowViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A profil ViewModelje.
    /// </summary>
    internal class ProfilWindowViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfilWindowViewModel"/> class.
        /// </summary>
        /// <param name="felh">Az itt megadott felhasználó profilját adja ki.</param>
        public ProfilWindowViewModel(Data.felhasznalo felh)
        {
            this.Nev = felh.nev;
            this.Felhnev = felh.felhasznalonev;
            this.Email = felh.email;
            this.Kupon = (int)felh.kupon;
            this.AdminE = (int)felh.adminisztratore;
        }

        /// <summary>
        /// Gets or sets név.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets felhasználónév.
        /// </summary>
        public string Felhnev { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets kupon.
        /// </summary>
        public int Kupon { get; set; }

        /// <summary>
        /// Gets or sets admin.
        /// </summary>
        public int AdminE { get; set; }

        /// <summary>
        /// Gets szerepkör.
        /// </summary>
        public string Szerepkor
        {
            get
            {
                if (this.AdminE == 0)
                {
                    return "Felhasználó";
                }
                else
                {
                    return "Adminisztrátor";
                }
            }
        }

        /// <summary>
        /// Ha még nincs kuponja a felhasználónak, kap egyet.
        /// </summary>
        /// <returns>Ha nincs még neki, akkor igaz.</returns>
        public bool KupontVesz()
        {
            return Logic.WeBestSellerBL.KupontVasarol();
        }
    }
}