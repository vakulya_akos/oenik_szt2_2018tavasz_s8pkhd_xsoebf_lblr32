﻿// <copyright file="Adminisztrálás.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System.Windows;
    using GUI;

    /// <summary>
    /// Interaction logic for Adminisztrálás.xaml
    /// </summary>
    public partial class Adminisztrálás : Window
    {
        private AdminisztralasViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Adminisztrálás"/> class.
        /// </summary>
        public Adminisztrálás()
        {
            this.InitializeComponent();
            this.vM = new AdminisztralasViewModel();
            this.DataContext = this.vM;
        }

        private void Uj_admin_Click(object sender, RoutedEventArgs e)
        {
            ÚjAdmin ua = new ÚjAdmin();
            ua.ShowDialog();
        }

        private void Elfogad_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.KijeloltKonyv != null)
            {
                this.vM.Elfogad(this.vM.KijeloltKonyv);

                // Böngészés bn = new Böngészés();
                // bn.ShowDialog();
                this.vM.ElbiralatlanKonyvek.Remove(this.vM.KijeloltKonyv);
                MessageBox.Show("Könyv Elfogadva!");
            }
            else
            {
                MessageBox.Show("Jelölj ki egy könyvet!");
            }
        }

        private void Elutasit_Click(object sender, RoutedEventArgs e)
        {
            this.vM.Elutasit(this.vM.KijeloltKonyv);
            this.vM.ElbiralatlanKonyvek.Remove(this.vM.KijeloltKonyv);
            MessageBox.Show("Könyv Elutasítva!");
        }
    }
}
