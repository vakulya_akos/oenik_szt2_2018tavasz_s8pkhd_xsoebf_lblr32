﻿// <copyright file="ÚjAdmin.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;

    /// <summary>
    /// Interaction logic for ÚjAdmin.xaml
    /// </summary>
    public partial class ÚjAdmin : Window
    {
        private AdminisztralasViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="ÚjAdmin"/> class.
        /// </summary>
        public ÚjAdmin()
        {
            this.InitializeComponent();
            this.vM = new AdminisztralasViewModel();
            this.DataContext = this.vM;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.tb_nev.Text != string.Empty && this.tb_email.Text != string.Empty)
            {
                bool sikerese = this.vM.AdmintEngedelyez(this.tb_nev.Text, this.tb_email.Text);
                if (sikerese)
                {
                    MessageBox.Show("Elfogadva");
                }
                else
                {
                    MessageBox.Show("Nem elfogadva, nincs ilyen felhasználó vagy már admin!");
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("Adj meg minden adatot!");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
