﻿// <copyright file="Regisztráció.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;

    /// <summary>
    /// Interaction logic for Regisztráció.xaml
    /// </summary>
    public partial class Regisztráció : Window
    {
        private RegisztracioViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Regisztráció"/> class.
        /// </summary>
        public Regisztráció()
        {
            this.InitializeComponent();
            this.vM = new RegisztracioViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Bejelentkezés az új accountal
            // Bejelentkezés bejelentkezes = new Bejelentkezés();
            // bejelentkezes.ShowDialog();
            if (this.vM.SikeresRegisztracio(this.tb_nev.Text, this.tb_jelszo.Password.ToString(), this.tb_email.Text, this.tb_felhasznalonev.Text) == true)
            {
                Bejelentkezés bejelentkezes = new Bejelentkezés();
                this.Close();
                bejelentkezes.ShowDialog();
            }
            else
            {
                MessageBox.Show("Hibás Regisztráció!");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
