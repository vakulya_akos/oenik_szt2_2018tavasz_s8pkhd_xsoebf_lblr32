﻿// <copyright file="Böngészés.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Data;
    using GUI;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Interaction logic for Böngészés.xaml
    /// </summary>
    public partial class Böngészés : Window
    {
        private BongeszesViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Böngészés"/> class.
        /// </summary>
        public Böngészés()
        {
            this.InitializeComponent();

            // BF = new BejelentkezettFelhasznalo();
            this.vM = new BongeszesViewModel();

            // lb_kinalat.ItemsSource = BF.FeltoltottKonyvek();
            this.DataContext = this.vM;

            // this.lb_kinalat.ItemsSource = this.VM.FeltoltottKonyvek();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // keresés
            if (this.tb_kereses.Text != string.Empty)
            {
                this.vM.Keres(this.tb_kereses.Text);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Kosárba helyez
            this.vM.KosarbaHelyezes();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            // Tovább
            Kosár kosar = new Kosár();
            kosar.ShowDialog();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
                Thread newWindowThread = new Thread(new ThreadStart(() =>
                {
                    Feltöltés feltoltes = new Feltöltés();
                    feltoltes.ShowDialog();
                    System.Windows.Threading.Dispatcher.Run();
                }));

            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
        }

        private void Button_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            // label_ajanlat.Content = "asd";
            foreach (var item in this.vM.MegvasaralhatoKonyvek)
            {
              // label_ajanlat.Content = item.Cim.ToString();
            }
        }

        private void Button_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Random r = new Random();
            if (e.ClickCount >= 3)
            {
                foreach (var item in this.vM.MegvasaralhatoKonyvek)
                {
                    // label_ajanlat.Content = item.Cim.ToString();
                }

                // label_ajanlat.Content = "Trónok harca";
            }
        }

        private void ReszletekButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.vM.Kijelolt_konyv != null)
            {
                Reszletek reszl = new Reszletek(this.vM.Kijelolt_konyv);
                reszl.Show();
            }
            else
            {
                MessageBox.Show("Jelölj ki egy könyvet!");
            }
        }

        private void ReszletekButton_Copy2_Click(object sender, RoutedEventArgs e)
        {
            PublikaltWindow pw = new PublikaltWindow();
            pw.Show();
        }

        private void ReszletekButton_Copy1_Click(object sender, RoutedEventArgs e)
        {
            KapcsolatWindow kw = new KapcsolatWindow();
            kw.Show();
        }

        private void ReszletekButton_Copy_Click(object sender, RoutedEventArgs e)
        {
            BejelentkezettFelhasznalo bf = new BejelentkezettFelhasznalo();
            ProfilWindow pw = new ProfilWindow(bf.ActualUser);
            pw.Show();
        }
    }
}
