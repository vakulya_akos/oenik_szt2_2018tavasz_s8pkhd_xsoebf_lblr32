﻿// <copyright file="ReszletekViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Részletek ViewModelje.
    /// </summary>
    internal class ReszletekViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReszletekViewModel"/> class.
        /// </summary>
        /// <param name="konyv">A könyv aminek a részleteit mutatja.</param>
        public ReszletekViewModel(Data.konyvek konyv)
        {
            this.Cim = konyv.Cim;
            this.Szerzo = konyv.szerzo;
            this.Mufaj = konyv.mufaj;
            this.ISBN = konyv.ISBN;
            this.Ev = (int)konyv.ev;
        }

        /// <summary>
        /// Gets or sets cim.
        /// </summary>
        public string Cim { get; set; }

        /// <summary>
        /// Gets or sets szerző.
        /// </summary>
        public string Szerzo { get; set; }

        /// <summary>
        /// Gets or sets műfaj.
        /// </summary>
        public string Mufaj { get; set; }

        /// <summary>
        /// Gets or sets ISBN.
        /// </summary>
        public string ISBN { get; set; }

        /// <summary>
        /// Gets or sets év.
        /// </summary>
        public int Ev { get; set; }
    }
}
