﻿// <copyright file="Kosár.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;
    using Logic;

    /// <summary>
    /// Interaction logic for Kosár.xaml
    /// </summary>
    public partial class Kosár : Window
    {
        private KosarViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Kosár"/> class.
        /// </summary>
        public Kosár()
        {
            this.InitializeComponent();
            this.vM = new KosarViewModel();
            this.DataContext = this.vM;

            if (this.vM.Osszeg == 0)
            {
                this.fizetes.IsEnabled = false;
            }

            if (this.vM.Kuponertek == 0)
            {
                this.ajandekkupon.IsEnabled = false;
            }

            this.label2.Content = "A kuponod értéke: " + this.vM.Kuponertek + "%";
        }

        private void Ajandekkupon_Click(object sender, RoutedEventArgs e)
        {
            this.osszegLabel.Content = WeBestSellerBL.KuponArOsszegzes((int)this.osszegLabel.Content);
            this.ajandekkupon.IsEnabled = false;
        }

        private void Fizetes_Click(object sender, RoutedEventArgs e)
        {
                FizetesWindow f = new FizetesWindow();
                this.Close();
                f.ShowDialog();

                // ide még lehetne egy kosár kiűrítés
                // megvásároltakhoz hozzáad
        }
    }
}
