﻿// <copyright file="Reszletek.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Reszletek.xaml
    /// </summary>
    public partial class Reszletek : Window
    {
        private ReszletekViewModel rVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Reszletek"/> class.
        /// </summary>
        /// <param name="konyv">A könyv aminek a részleteit mutatja.</param>
        public Reszletek(Data.konyvek konyv)
        {
            this.rVM = new ReszletekViewModel(konyv);
            this.InitializeComponent();
            this.DataContext = this.rVM;
        }
    }
}
