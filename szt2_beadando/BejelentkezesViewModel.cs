﻿// <copyright file="BejelentkezesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Szerepkör lehetőségeket tartalmazó enum.
    /// </summary>
    internal enum SzerepkorLehetosegek
    {
        /// <summary>
        /// Adminsztrátor szerepkör.
        /// </summary>
        Adminisztrátor,

        /// <summary>
        /// Felhasználó szerepkör.
        /// </summary>
        Felhasználó
    }

    /// <summary>
    /// Bejelentkezés ablak ViewModelje.
    /// </summary>
    public class BejelentkezesViewModel
    {
        private Bejelentkezes bl;

        /// <summary>
        /// Initializes a new instance of the <see cref="BejelentkezesViewModel"/> class.
        /// </summary>
        public BejelentkezesViewModel()
        {
            this.bl = new Bejelentkezes();
        }

        /// <summary>
        /// Bejelentkezési adatokat ellenőrzi.
        /// </summary>
        /// <param name="name">A felhasználó neve.</param>
        /// <param name="pw">A felhasználó jelszava.</param>
        /// <returns>Igaz ha sikeres, hamis ha nem.</returns>
         public bool SikeresBejelentkezes(string name, string pw)
        {
            if (this.bl.SikeresBejelentkezes(name, pw) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Ellenőrzi hogy adminként vagy felhasználóként jelentkezett be.
        /// </summary>
        /// <returns>Igaz ha admin.</returns>
        public bool Adminkent()
        {
            return WeBestSellerBL.AdminE();
        }
    }
}
