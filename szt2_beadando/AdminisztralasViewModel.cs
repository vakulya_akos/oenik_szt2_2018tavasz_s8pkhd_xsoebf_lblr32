﻿// <copyright file="AdminisztralasViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Adminisztrálás ablak ViewModelje
    /// </summary>
    public class AdminisztralasViewModel
    {
        private AdminisztratotFelulet adminF;
        private BejelentkezettFelhasznalo bL;
        private konyvek kijeloltKonyv;
        private ObservableCollection<konyvek> elbiralatlanKonyvek;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminisztralasViewModel"/> class.
        /// </summary>
        public AdminisztralasViewModel()
        {
            this.adminF = new AdminisztratotFelulet();
            this.bL = new BejelentkezettFelhasznalo();
            this.kijeloltKonyv = new konyvek();
            this.elbiralatlanKonyvek = new ObservableCollection<konyvek>();

            foreach (konyvek item in this.bL.FeltoltottKonyvek())
            {
                if (item.engedelyezvee == 0)
                {
                    this.ElbiralatlanKonyvek.Add(item);
                }
            }
        }

        /// <summary>
        /// Gets or sets kijelöltKönyv
        /// </summary>
        public konyvek KijeloltKonyv
        {
            get
            {
                return this.kijeloltKonyv;
            }

            set
            {
                this.kijeloltKonyv = value;
            }
        }

        /// <summary>
        /// Gets or sets elbiralatlakKönyvek
        /// </summary>
        public ObservableCollection<konyvek> ElbiralatlanKonyvek
        {
            get
            {
                return this.elbiralatlanKonyvek;
            }

            set
            {
                this.elbiralatlanKonyvek = value;
            }
        }

        /// <summary>
        /// Admint hozzáad.
        /// </summary>
        /// <param name="nev">Új admin neve.</param>
        /// <param name="email">Új admin email címe.</param>
        /// <returns>Adminná tesz metódus.</returns>
        public bool AdmintEngedelyez(string nev, string email)
        {
           return WeBestSellerBL.AdminnaTesz(nev, email);
        }

        /// <summary>
        /// ELfogadja a kijelölt könyvet.
        /// </summary>
        /// <param name="konyv">A kijelölt könyv.</param>
        public void Elfogad(konyvek konyv)
        {
            this.adminF.KonyvElfogadas(konyv);
        }

        /// <summary>
        /// Elutasítja a kijelölt könyvet.
        /// </summary>
        /// <param name="konyv">A kijelölt könyv.</param>
        public void Elutasit(konyvek konyv)
        {
            this.adminF.KonyvElutasitas(konyv);
            this.ElbiralatlanKonyvek.Remove(konyv);
        }
    }
}
