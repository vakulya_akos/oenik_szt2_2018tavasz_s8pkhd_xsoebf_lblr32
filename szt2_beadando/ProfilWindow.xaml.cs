﻿// <copyright file="ProfilWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ProfilWindow.xaml
    /// </summary>
    public partial class ProfilWindow : Window
    {
        private ProfilWindowViewModel pwvm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfilWindow"/> class.
        /// </summary>
        /// <param name="felh">Az itt megadott felhasználó profilját adja ki.</param>
        public ProfilWindow(Data.felhasznalo felh)
        {
            this.InitializeComponent();
            this.pwvm = new ProfilWindowViewModel(felh);
            this.DataContext = this.pwvm;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
           bool na = this.pwvm.KupontVesz();
            if (na)
            {
                MessageBox.Show("Kuponvásárlás Sikeres");
            }
            else
            {
                MessageBox.Show("Kuponvásárlás Sikertelen, Már van Kuponod!");
            }

            this.button1.IsEnabled = false;
        }
    }
}
