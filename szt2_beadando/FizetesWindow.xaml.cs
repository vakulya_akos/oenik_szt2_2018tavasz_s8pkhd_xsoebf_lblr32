﻿// <copyright file="FizetesWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Logic;

    /// <summary>
    /// Interaction logic for FizetesWindow.xaml
    /// </summary>
    public partial class FizetesWindow : Window
    {
        private int osszesitettOsszeg;
        private FizetesViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizetesWindow"/> class.
        /// </summary>
        public FizetesWindow()
        {
            this.vM = new FizetesViewModel();
            this.InitializeComponent();
            this.osszesitettOsszeg = WeBestSellerBL.ArOsszegzes(WeBestSellerBL.KosarbanLevoKonyvek);
            this.osszegLabel.Content = (int)WeBestSellerBL.KuponArOsszegzes(this.osszesitettOsszeg);
            WeBestSellerBL.KupontElvesz();
        }

        /// <summary>
        /// Gets or sets osszesitettOsszeg
        /// </summary>
        public int OsszesitettOsszeg
        {
            get
            {
                return this.osszesitettOsszeg;
            }

            set
            {
                this.osszesitettOsszeg = value;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.PinKodtextBox.Text != string.Empty && this.KartyaSzamtextBox.Text != string.Empty)
            {
                Random r = new Random();
                string kozep = r.Next(1000, 2000).ToString();

                this.vM.VasarlashozHozzaAd();
                this.cim.Content = "Letöltő link:";
                this.link.Content = "www." + kozep + "_webestsellerebook.com";
                this.button.IsEnabled = false;
            }
            else
            {
                this.link.Content = "ADJ MEG MINDEN ADATOT!";
            }
        }
    }
}
