﻿// <copyright file="BongeszesViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using GUI;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Böngészés ablak ViewModelje
    /// </summary>
    public class BongeszesViewModel : Bindable
    {
        private BejelentkezettFelhasznalo bL;

        private konyvek kijeloltKonyv;
        private ObservableCollection<konyvek> keresettKonyvek;
        private ObservableCollection<konyvek> megvasaralhatoKonyvek;
        private ObservableCollection<konyvek> kosarbanlevoKonyvek;

        /// <summary>
        /// Initializes a new instance of the <see cref="BongeszesViewModel"/> class.
        /// </summary>
        public BongeszesViewModel()
        {
            this.bL = new BejelentkezettFelhasznalo();
            this.keresettKonyvek = new ObservableCollection<konyvek>();

            this.megvasaralhatoKonyvek = new ObservableCollection<konyvek>();
            foreach (konyvek item in this.bL.FeltoltottKonyvek())
            {
                if (item.engedelyezvee == 1)
                {
                    this.megvasaralhatoKonyvek.Add(item);
                }
            }

            this.kosarbanlevoKonyvek = new ObservableCollection<konyvek>();
        }

        /// <summary>
        /// Gets or sets kijeloltKonyv.
        /// </summary>
        public konyvek Kijelolt_konyv
        {
            get
            {
                return this.kijeloltKonyv;
            }

            set
            {
                this.kijeloltKonyv = value;
            }
        }

        /// <summary>
        /// Gets or sets keresettKonyv.
        /// </summary>
        public ObservableCollection<konyvek> KeresettKonyvek
        {
            get
            {
                return this.keresettKonyvek;
            }

            set
            {
                this.keresettKonyvek = value;
            }
        }

        /// <summary>
        /// Gets or sets megvasarolhatoKonyvek.
        /// </summary>
        public ObservableCollection<konyvek> MegvasaralhatoKonyvek
        {
            get
            {
                return this.megvasaralhatoKonyvek;
            }

            set
            {
                this.megvasaralhatoKonyvek = value;
            }
        }

        /// <summary>
        /// Gets or sets kosarbanlevoKonyvek.
        /// </summary>
        public ObservableCollection<konyvek> KosarbanlevoKonyvek
        {
            get
            {
                // return BL.MegvasaroltKonyvek();
                return this.kosarbanlevoKonyvek;
            }

            set
            {
                this.kosarbanlevoKonyvek = value;
            }
        }

        /// <summary>
        /// Cím alapján kikeresi a keresett könyvet.
        /// </summary>
        /// <param name="cim">Ami alapján keresünk.</param>
        public void Keres(string cim)
        {
            this.keresettKonyvek.Clear();

            if (this.MegvasaralhatoKonyvek != null)
            {
                foreach (konyvek item in this.MegvasaralhatoKonyvek)
                {
                    if (item.Cim.ToUpper().Contains(cim.ToUpper()))
                    {
                        this.KeresettKonyvek.Add(item);
                    }
                }
            }
        }

        /// <summary>
        /// A kijelölt könyvet kosárba helyezi.
        /// </summary>
        public void KosarbaHelyezes()
        {
            if (this.Kijelolt_konyv != null)
            {
               WeBestSellerBL.KosarbanLevoKonyvek.Add(this.kijeloltKonyv);

               // BL.ActualUser.kosar.Add(Kijelolt_konyv);
               this.KosarbanlevoKonyvek.Add(this.Kijelolt_konyv);
               this.MegvasaralhatoKonyvek.Remove(this.Kijelolt_konyv);
           }
        }
    }
}