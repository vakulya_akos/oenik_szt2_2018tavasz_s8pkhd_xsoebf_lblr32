﻿// <copyright file="KapcsolatWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for KapcsolatWindow.xaml
    /// </summary>
    public partial class KapcsolatWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KapcsolatWindow"/> class.
        /// </summary>
        public KapcsolatWindow()
        {
            this.InitializeComponent();
        }

        private void Button_Copy_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string richText = new TextRange(this.tb_uzenet.Document.ContentStart, this.tb_uzenet.Document.ContentEnd).Text;
                if (this.tb_email.Text != string.Empty && this.tb_targy.Text != string.Empty && richText != string.Empty)
                {
                    this.EmailSender(this.tb_email.Text, this.tb_targy.Text, richText);
                    this.button.IsEnabled = false;
                    this.warning.Content = "SIKERES KÜLDÉS!";
                }
                else
                {
                    this.warning.Content = "Valami hiányzik!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Az email küldést valósítja meg.
        /// </summary>
        /// <param name="email">Email cím amiről küldjük az üzenetet.</param>
        /// <param name="targy">Az üzenet tárgya.</param>
        /// <param name="uzenet">Maga az üzenet.</param>
        private void EmailSender(string email, string targy, string uzenet)
        {
            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("webestseller@gmail.com", "ebook001"),
                EnableSsl = true
            };

            targy = targy + " --- Küldő email címe:  " + email + " --- Küldő Neve: " + Logic.WeBestSellerBL.Felhasznalo;
            client.Send(email, "webestseller@gmail.com", targy, uzenet);
        }
    }
}
