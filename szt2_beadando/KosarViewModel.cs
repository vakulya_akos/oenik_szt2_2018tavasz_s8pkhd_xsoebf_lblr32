﻿// <copyright file="KosarViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using GUI;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// A kosár ViewModelje.
    /// </summary>
    public class KosarViewModel
    {
        // private KonyvAdatlap bL;
        private ObservableCollection<konyvek> kosar;
        private BongeszesViewModel bVM;
        private int osszeg;
        private int kuponertek;

        /// <summary>
        /// Initializes a new instance of the <see cref="KosarViewModel"/> class.
        /// </summary>
        public KosarViewModel()
        {
            // this.bL = new KonyvAdatlap();
            this.bVM = new BongeszesViewModel();
            this.kosar = new ObservableCollection<konyvek>();
            this.kuponertek = WeBestSellerBL.KuponErtek();

            foreach (konyvek item in WeBestSellerBL.KosarbanLevoKonyvek)
            {
                this.kosar.Add(item);
            }

            this.osszeg = WeBestSellerBL.ArOsszegzes(WeBestSellerBL.KosarbanLevoKonyvek);
        }

        /// <summary>
        /// Gets or sets kosar.
        /// </summary>
        public ObservableCollection<konyvek> Kosar
        {
            get
            {
                return this.kosar;
            }

            set
            {
                this.kosar = value;
            }
        }

        /// <summary>
        /// Gets or sets kuponérték.
        /// </summary>
        public int Kuponertek
        {
            get { return this.kuponertek; }
            set { this.kuponertek = value; }
        }

        /// <summary>
        /// Gets or sets osszeg.
        /// </summary>
        public int Osszeg
        {
            get
            {
                return this.osszeg;
            }

            set
            {
                this.osszeg = value;
            }
        }
    }
}
