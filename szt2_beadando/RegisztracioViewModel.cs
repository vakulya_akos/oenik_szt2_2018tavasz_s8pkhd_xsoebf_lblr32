﻿// <copyright file="RegisztracioViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Logic;
    using Logic.Interfaces;

    /// <summary>
    /// Regisztrálás ViewModelje.
    /// </summary>
    public class RegisztracioViewModel
    {
        private Regisztracio bL;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisztracioViewModel"/> class.
        /// </summary>
        public RegisztracioViewModel()
        {
            this.bL = new Regisztracio();
        }

        /// <summary>
        /// Ellenőrzi, hogy sikeres-e a regisztráció.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="jelszo">A felhasználó jelszó paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <param name="felhasznalonev">A felhasználó felhasználónév paramétere.</param>
        /// <returns>Igaz, ha sikeres.</returns>
        public bool SikeresRegisztracio(string nev, string jelszo, string email, string felhasznalonev)
        {
            if (this.bL.RegisztracioSikeres(nev, felhasznalonev, email, jelszo) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
