﻿// <copyright file="Feltöltés.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GUI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GUI;

    /// <summary>
    /// Interaction logic for Feltöltés.xaml
    /// </summary>
    public partial class Feltöltés : Window
    {
        private FeltoltesViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Feltöltés"/> class.
        /// </summary>
        public Feltöltés()
        {
            this.InitializeComponent();
            this.vM = new FeltoltesViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.tb_ISBN.Text != string.Empty && this.tb_cim.Text != string.Empty && this.tb_ar.Text != string.Empty && this.tb_mufaj.Text != string.Empty && this.tb_ev.Text != string.Empty && this.tb_szerzo.Text != string.Empty)
            {
                if (this.vM.SikeresFeltoltes(this.tb_ISBN.Text, this.tb_szerzo.Text, this.tb_cim.Text, this.tb_mufaj.Text, int.Parse(this.tb_ev.Text), int.Parse(this.tb_ar.Text)) == true)
                {
                    MessageBox.Show("Sikeres");
                    this.vM.PublikalhozHozzaad(this.tb_ISBN.Text);
                }
                else
                {
                    MessageBox.Show("Hiba A feltöltésben");
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("Adj meg minden adatot");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
