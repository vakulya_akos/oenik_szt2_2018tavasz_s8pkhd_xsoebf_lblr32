﻿// <copyright file="WeBestSellerBL.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    /// <summary>
    /// Központi logika osztály.
    /// </summary>
    public class WeBestSellerBL
    {
        private static string felhasznalo;
        private static List<konyvek> kosarbanLevoKonyvek = new List<konyvek>();
        private static DatabaseRepo dbr = new DatabaseRepo();

        /// <summary>
        /// Gets or sets felhasznalo.
        /// </summary>
        public static string Felhasznalo
        {
            get
            {
                return felhasznalo;
            }

            set
            {
                felhasznalo = value;
            }
        }

        /// <summary>
        /// Gets or sets kosarbanlevoKonyvek.
        /// </summary>
        public static List<konyvek> KosarbanLevoKonyvek
        {
            get
            {
                return kosarbanLevoKonyvek;
            }

            set
            {
                kosarbanLevoKonyvek = value;
            }
        }

        /// <summary>
        /// A kiválasztott felhasználót adminná teszi.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <returns>Igaz, ha admin lett.</returns>
        public static bool AdminnaTesz(string nev, string email)
        {
            // ha létezik ilyen felh, és még nem admin --> admint elfogadja
            if (!AdminEMarAzAdminnaTeendo(nev, email) && AdminnaTeendoVanE(nev, email))
            {
                dbr.AdminElfogad(nev, email);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// A megadott listában összegzi a könyvek árát.
        /// </summary>
        /// <param name="konyvek">A megadott lista.</param>
        /// <returns>Visszaadja a könyvek árának összegét.</returns>
        public static int ArOsszegzes(List<konyvek> konyvek)
        {
            int osszeg = 0;
            foreach (konyvek item in konyvek)
            {
                osszeg += (int)item.ar;
            }

            return osszeg;
        }

        /// <summary>
        /// Megadja a felhasználó kuponértékét.
        /// </summary>
        /// <returns>A kuponértéket adja vissza.</returns>
        public static int KuponErtek()
        {
             return (int)dbr.GetAllfelhasznalo().Where(x => x.felhasznalonev == WeBestSellerBL.Felhasznalo).First().kupon;
        }

        /// <summary>
        /// Az összeget csökkenti a kupon értéke szerint.
        /// </summary>
        /// <param name="osszeg">Az összeg amit csökkenteni fog.</param>
        /// <returns>A csökkentett értékű ár.</returns>
        public static double KuponArOsszegzes(int osszeg)
        {
            double kuponosOsszeg = osszeg;
            if (dbr.GetAllfelhasznalo().Where(x => x.felhasznalonev == WeBestSellerBL.Felhasznalo).First().kupon != 0)
            {
                kuponosOsszeg = osszeg - ((osszeg / 100) * (double)dbr.GetAllfelhasznalo().Where(x => x.felhasznalonev == WeBestSellerBL.Felhasznalo).First().kupon);
            }

            return kuponosOsszeg;
        }

        /// <summary>
        /// Visszaadja, hogy a felhasználó admin-e.
        /// </summary>
        /// <returns>Igaz, ha admin.</returns>
        public static bool AdminE()
        {
            int s = (int)dbr.GetAllfelhasznalo().Where(x => x.felhasznalonev == WeBestSellerBL.Felhasznalo).First().adminisztratore;
            if (s == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Meghívja az adatbázis Betudjelentkezni metódusát.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="pw">A felhasználó jelszó paramétere.</param>
        /// <returns>Igaz, ha be tud jelentkezni.</returns>
        public static bool BetudJelentkezni(string name, string pw)
        {
            dbr = new DatabaseRepo();
            return dbr.BetuDjelentkezni(name, pw);
        }

        /// <summary>
        /// A megadott felhasználót az adatbázishoz adja.
        /// </summary>
        /// <param name="felh">Maga a felhasználó.</param>
        public static void AdatbazishozHozzaAd(felhasznalo felh)
        {
            dbr.AdatbazishozHozzaAd(felh);
        }

        /// <summary>
        /// A megadott könyvet az adatbázishoz adja.
        /// </summary>
        /// <param name="konyv">Maga a könyv.</param>
        public static void AdatbazishozHozzaAdKonyv(konyvek konyv)
        {
            dbr.KonyvAdatbazishozHozzaAd(konyv);
        }

        /// <summary>
        /// Megnézi, hogy van-e ilyen felhasználó.
        /// </summary>
        /// <param name="name">A felhasználó neve.</param>
        /// <returns>Igaz, ha van ilyen felhasználó.</returns>
        public static bool FElhasznaloVanE(string name)
        {
            return dbr.VanEIlyenFElhasznalo(name);
        }

        /// <summary>
        /// Megnézi, hogy van-e ilyen könyv.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        /// <returns>Igaz, ha van ilyen könyv.</returns>
        public static bool KonyvVanE(string isbn)
        {
            return dbr.VanEIlyenKonyv(isbn);
        }

        /// <summary>
        /// Megnézi, hogy a felhasználó létezik-e.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <returns>Ha létezik, akkor igaz.</returns>
        public static bool AdminnaTeendoVanE(string name, string email)
        {
             return dbr.VaneilyenAdminnaTeendo(name, email);
        }

        /// <summary>
        /// Megnézi, hogy a felhasználó admin-e.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <returns>Ha admin már, akkor igaz.</returns>
        public static bool AdminEMarAzAdminnaTeendo(string name, string email)
        {
            return dbr.AdminEmar(name, email);
        }

        /// <summary>
        /// Kupont ad a felhasználónak.
        /// </summary>
        /// <returns>Ha nincs kuponja, ad neki.</returns>
        public static bool KupontVasarol()
        {
            Random r = new Random();
            if (dbr.VanKuponja(WeBestSellerBL.Felhasznalo))
            {
                return false;
            }
            else
            {
                dbr.KupontHozzaAd(WeBestSellerBL.Felhasznalo, r.Next(1, 49));
                return true;
            }
        }

        /// <summary>
        /// Elveszi a felhasználó kuponját.
        /// </summary>
        public static void KupontElvesz()
        {
            dbr.KupontElvesz(WeBestSellerBL.Felhasznalo);
        }

        /// <summary>
        /// Visszaadja a felhasználó vásárlásait.
        /// </summary>
        /// <returns>A felhasználó vásárlásai.</returns>
        public static List<vasarol> AktualisFelhVasarlasai()
        {
            return dbr.Vasarlasok(WeBestSellerBL.Felhasznalo);
        }

        /// <summary>
        /// Visszaadja a felhasználó publikálásait.
        /// </summary>
        /// <returns>A felhasználó publikálásai.</returns>
        public static List<publikal> AktualisFelhPublikálásai()
        {
            return dbr.Publikalasok(WeBestSellerBL.Felhasznalo);
        }

        /// <summary>
        /// Egy új vásárlást hozzáad a vásárlásokhoz.
        /// </summary>
        public void VasarlashozHozzaAd()
        {
            vasarol ujvasarlas;
            foreach (var konyv in WeBestSellerBL.KosarbanLevoKonyvek)
            {
                ujvasarlas = new vasarol();
                ujvasarlas.ISBN = konyv.ISBN;
                ujvasarlas.email = dbr.BejelentkezettFelhasznalo(WeBestSellerBL.Felhasznalo).email;
                ujvasarlas.nev = dbr.BejelentkezettFelhasznalo(WeBestSellerBL.Felhasznalo).nev;
                ujvasarlas.vasarlas_id = dbr.GetAllvasarol().Last().vasarlas_id + 1;

                dbr.VasarlasAdatbazishozHozzaAd(ujvasarlas);
            }
        }

        /// <summary>
        /// Egy meglévő vásárlást elvesz a vásárlásokból.
        /// </summary>
        /// <param name="tesztnev">Az eltávolítandó vásárlás.</param>
        public void VasarlasbolEltavolit(string tesztnev)
        {
            dbr.VasarlasAdatbazisbolEltavolit(tesztnev);
        }
    }
}
