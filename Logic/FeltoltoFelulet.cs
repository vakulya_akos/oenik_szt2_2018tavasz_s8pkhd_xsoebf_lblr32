﻿// <copyright file="FeltoltoFelulet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic.Interfaces;

    /// <summary>
    /// A feltöltőfelület logic osztálya.
    /// </summary>
    public class FeltoltoFelulet : IFeltoltoFelulet
    {
        private static DatabaseRepo dbr;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeltoltoFelulet"/> class.
        /// </summary>
        public FeltoltoFelulet()
        {
            dbr = new DatabaseRepo();
        }

        /// <summary>
        /// Ellenőrzi, hogy a feltöltés sikeres-e.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        /// <param name="szerzo">A könyv szerző paramétere.</param>
        /// <param name="cim">A könyv cím paramétere.</param>
        /// <param name="mufaj">A könyv műfaj paramétere.</param>
        /// <param name="ev">A könyv év paramétere.</param>
        /// <param name="ar">A könyv ár paramétere.</param>
        /// <returns>Igaz, ha sikeres.</returns>
        public bool FeltoltesSikeres(string isbn, string szerzo, string cim, string mufaj, int ev, int ar)
        {
            // message = string.Empty;
            if (isbn == string.Empty || szerzo == string.Empty || cim == string.Empty || mufaj == string.Empty || ev == 0 || ar == 0)
            {
                // message = "Minden adatot kikell tölteni!";
                return false;
            }

            if (WeBestSellerBL.KonyvVanE(isbn))
            {
                // message += nev + "nevű felhasználó már van!";
                return false;
            }

            this.KonyvetFeltolt(isbn, szerzo, cim, mufaj, ev, ar);

            // message += "Sikeres Regisztráció!";
            return true;
        }

        /// <summary>
        /// Létrehoz egy új könyvet a megadott adatokból.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        /// <param name="szerzo">A könyv szerző paramétere.</param>
        /// <param name="cim">A könyv cím paramétere.</param>
        /// <param name="mufaj">A könyv műfaj paramétere.</param>
        /// <param name="ev">A könyv év paramétere.</param>
        /// <param name="ar">A könyv ár paramétere.</param>
        public void KonyvetFeltolt(string isbn, string szerzo, string cim, string mufaj, int ev, int ar)
        {
            konyvek ujkonyv = new konyvek();
            ujkonyv.ISBN = isbn;
            ujkonyv.szerzo = szerzo;
            ujkonyv.cim = cim;
            ujkonyv.mufaj = mufaj;
            ujkonyv.ev = ev;
            ujkonyv.ar = ar;
            ujkonyv.engedelyezvee = 0;
            WeBestSellerBL.AdatbazishozHozzaAdKonyv(ujkonyv);
        }

        /// <summary>
        /// A publikálás adatbázihoz adj a könyvet.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        public void KonyvetHozzaad(string isbn)
        {
            publikal ujpublikalas = new publikal();
            ujpublikalas.ISBN = isbn;
            ujpublikalas.email = dbr.BejelentkezettFelhasznalo(WeBestSellerBL.Felhasznalo).email;
            ujpublikalas.nev = dbr.BejelentkezettFelhasznalo(WeBestSellerBL.Felhasznalo).nev;
            ujpublikalas.publikalas_id = dbr.GetAllpublikal().Last().publikalas_id + 1;

            dbr.FeltoltesAdatbazishozHozzaad(ujpublikalas);
        }
    }
}
