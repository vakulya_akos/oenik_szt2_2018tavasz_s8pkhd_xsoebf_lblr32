﻿// <copyright file="Regisztracio.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic.Interfaces;

    /// <summary>
    /// A regisztráció logic osztálya.
    /// </summary>
    public class Regisztracio : IRegisztracio
    {
        private static DatabaseRepo dbr;

        /// <summary>
        /// Initializes a new instance of the <see cref="Regisztracio"/> class.
        /// </summary>
        public Regisztracio()
        {
            dbr = new DatabaseRepo();
        }

        /// <summary>
        /// Ellenőrzi, hogy a regisztráció sikeres-e.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="felh">A felhasználó felhasználónév paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <param name="jelszo">A felhasználó jelszo paramétere.</param>
        /// <returns>Igaz, ha sikeres.</returns>
        public bool RegisztracioSikeres(string nev, string felh, string email, string jelszo)
        {
            // message = string.Empty;
            if (nev == string.Empty || email == string.Empty || felh == string.Empty || jelszo == string.Empty)
            {
                // message = "Minden adatot kikell tölteni!";
                return false;
            }

            if (WeBestSellerBL.FElhasznaloVanE(nev))
            {
                // message += nev + "nevű felhasználó már van!";
                return false;
            }

            ((IRegisztracio)this).Regisztracio(nev, felh, email, jelszo);

            // message += "Sikeres Regisztráció!";
            return true;
        }

        /// <summary>
        /// A felhasználót beregisztrálja az adatbázisba.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="felhasznalonev">A felhasználó felhasználónév paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <param name="jelszo">A felhasználó jelszó paramétere.</param>
        void IRegisztracio.Regisztracio(string nev, string felhasznalonev, string email, string jelszo)
        {
            felhasznalo ujfelh = new felhasznalo();
            ujfelh.nev = nev;
            ujfelh.felhasznalonev = felhasznalonev;
            ujfelh.email = email;
            ujfelh.jelszo = jelszo;
            ujfelh.kupon = 0;
            ujfelh.adminisztratore = 0;
            WeBestSellerBL.AdatbazishozHozzaAd(ujfelh);
        }
    }
}