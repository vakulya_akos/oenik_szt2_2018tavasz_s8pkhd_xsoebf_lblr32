﻿// <copyright file="IAdminisztratorFelulet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    /// <summary>
    /// Adminisztrátorfelület interfacei
    /// </summary>
   public interface IAdminisztratorFelulet
    {
        /// <summary>
        /// ELfogadja a kijelölt könyvet.
        /// </summary>
        /// <param name="konyv">A kijelölt könyv.</param>
        void KonyvElfogadas(konyvek konyv);
    }
}
