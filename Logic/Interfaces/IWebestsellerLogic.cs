﻿// <copyright file="IWebestsellerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Webestseller interfacei
    /// </summary>
    public interface IWebestsellerLogic
    {
        /// <summary>
        /// Bejelentkezteti a felhasználót.
        /// </summary>
        /// <param name="felhasznalonev">A felhasználó felhasználóneve.</param>
        /// <param name="jelszo">A felhasználó jelszava.</param>
        void Bejelentkezes(string felhasznalonev, string jelszo);

        /// <summary>
        /// Kijelentkezteti a felhasználót.
        /// </summary>
        void Kijelentkezes();
    }
}
