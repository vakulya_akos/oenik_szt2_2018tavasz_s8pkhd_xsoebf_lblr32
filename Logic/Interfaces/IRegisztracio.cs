﻿// <copyright file="IRegisztracio.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Regisztráció interfacei
    /// </summary>
    public interface IRegisztracio
    {
        /// <summary>
        /// A felhasználót beregisztrálja az adatbázisba.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="felhasznalonev">A felhasználó felhasználónév paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <param name="jelszo">A felhasználó jelszó paramétere.</param>
        void Regisztracio(string nev, string felhasznalonev, string email, string jelszo);

        /// <summary>
        /// Ellenőrzi, hogy a regisztráció sikeres-e.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="felh">A felhasználó felhasználónév paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <param name="jelszo">A felhasználó jelszo paramétere.</param>
        /// <returns>Igaz, ha sikeres.</returns>
        bool RegisztracioSikeres(string nev, string felh, string email, string jelszo);
    }
}
