﻿// <copyright file="IBejelentkezes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Bejelentkezés interfacei
    /// </summary>
    public interface IBejelentkezes
    {
        /// <summary>
        /// Ellenőrzi, hogy a bejelentkezés sikeres-e.
        /// </summary>
        /// <param name="name">A felhasznló neve.</param>
        /// <param name="pw">A felhasználó jelszava.</param>
        /// <returns>True, ha sikeres.</returns>
        bool SikeresBejelentkezes(string name, string pw);
    }
}
