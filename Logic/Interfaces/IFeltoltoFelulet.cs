﻿// <copyright file="IFeltoltoFelulet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Feltöltőfelület interfacei
    /// </summary>
    public interface IFeltoltoFelulet
    {
        /// <summary>
        /// Létrehoz egy új könyvet a megadott adatokból.
        /// </summary>
        /// <param name="isbn">A könyv ISBN paramétere.</param>
        /// <param name="szerzo">A könyv szerző paramétere.</param>
        /// <param name="cim">A könyv cím paramétere.</param>
        /// <param name="mufaj">A könyv műfaj paramétere.</param>
        /// <param name="ev">A könyv év paramétere.</param>
        /// <param name="ar">A könyv ár paramétere.</param>
        void KonyvetFeltolt(string isbn, string szerzo, string cim, string mufaj, int ev, int ar);
    }
}
