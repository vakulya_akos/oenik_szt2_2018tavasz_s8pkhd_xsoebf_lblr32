﻿// <copyright file="IBejelentkezettFelhasznalo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    /// <summary>
    /// Bejelentkezett felhasználó interfacei
    /// </summary>
    public interface IBejelentkezettFelhasznalo
    {
        /// <summary>
        /// Gets or sets email.
        /// </summary>
        string Email { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        string Felhasznalonev { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        string Jelszo { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        int Kupon { get; set; }

        /// <summary>
        /// Gets or sets email.
        /// </summary>
        string Nev { get; set; }

        /// <summary>
        /// Megnézi, hogy admin-e.
        /// </summary>
        /// <returns>Igaz, ha admin.</returns>
        bool AdminE();

        /// <summary>
        /// Feltöltött könyvek listája
        /// </summary>
        /// <returns>Feltöltött könyvek listája.</returns>
        List<konyvek> FeltoltottKonyvek();

        /// <summary>
        /// Megvásárolt könyvek listája
        /// </summary>
        /// <returns>Megvásárolt könyvek listája.</returns>
        List<konyvek> MegvasaroltKonyvek();
    }
}
