﻿// <copyright file="BejelentkezettFelhasznalo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic.Interfaces;

    /// <summary>
    /// A bejelentkezett felhasználóhoz tartozó logic osztály.
    /// </summary>
    public class BejelentkezettFelhasznalo
    {
        private DatabaseRepo dR = new DatabaseRepo();
        private felhasznalo actualUser;

        /// <summary>
        /// Initializes a new instance of the <see cref="BejelentkezettFelhasznalo"/> class.
        /// </summary>
        public BejelentkezettFelhasznalo()
        {
            this.ActualUser = this.dR.BejelentkezettFelhasznalo(WeBestSellerBL.Felhasznalo);
        }

        /// <summary>
        /// Gets or sets actualuser.
        /// </summary>
        public felhasznalo ActualUser
        {
            get
            {
                return this.actualUser;
            }

            set
            {
                this.actualUser = value;
            }
        }

        /// <summary>
        /// Visszaadja az adatbázisból a feltöltött könyveket.
        /// </summary>
        /// <returns>Feltöltött könyvek listája.</returns>
        public List<konyvek> FeltoltottKonyvek()
        {
            return this.dR.GetAllkonyvek();
        }

        // public List<konyvek> MegvasaroltKonyvek()
        // {
        //    throw new NotImplementedException();
        // }
    }
}
