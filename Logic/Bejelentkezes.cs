﻿// <copyright file="Bejelentkezes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic.Interfaces;

    /// <summary>
    /// A bejelentkezéshez tartozó logic osztály.
    /// </summary>
    public class Bejelentkezes : IBejelentkezes
    {
        private static DatabaseRepo dbr;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bejelentkezes"/> class.
        /// </summary>
        public Bejelentkezes()
        {
            dbr = new DatabaseRepo();
        }

        /// <summary>
        /// Ellenőrzi, hogy a bejelentkezés sikeres-e.
        /// </summary>
        /// <param name="name">A felhasznló neve.</param>
        /// <param name="pw">A felhasználó jelszava.</param>
        /// <returns>True, ha sikeres.</returns>
        public bool SikeresBejelentkezes(string name, string pw)
        {
            WeBestSellerBL.KosarbanLevoKonyvek = new List<konyvek>();
            if (WeBestSellerBL.BetudJelentkezni(name, pw))
            {
                WeBestSellerBL.Felhasznalo = name;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
