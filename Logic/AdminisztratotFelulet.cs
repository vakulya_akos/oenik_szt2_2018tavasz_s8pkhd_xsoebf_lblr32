﻿// <copyright file="AdminisztratotFelulet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Logic.Interfaces;

    /// <summary>
    /// Az adminisztrátorfelülethez tartozó logic osztály.
    /// </summary>
    public class AdminisztratotFelulet : IAdminisztratorFelulet
    {
        private DatabaseRepo dBR = new DatabaseRepo();

        /// <summary>
        /// Az adott könyvet az admin elfogadja.
        /// </summary>
        /// <param name="konyv">Az adott könyv.</param>
        public void KonyvElfogadas(konyvek konyv)
        {
            this.dBR.Elfogad(konyv);
        }

        /// <summary>
        /// Az adott könyvet az admin elutasítja.
        /// </summary>
        /// <param name="konyv">Az adott könyv.</param>
        public void KonyvElutasitas(konyvek konyv)
        {
            this.dBR.Elutasit(konyv);
        }
    }
}
