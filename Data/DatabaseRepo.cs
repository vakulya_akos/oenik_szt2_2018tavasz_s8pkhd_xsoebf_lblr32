﻿// <copyright file="DatabaseRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Az adatbázist kezelő osztály.
    /// </summary>
    public class DatabaseRepo : IDatabaseRepo
    {
        private static DatabaseEntities dB;

        /// <summary>
        /// Databaserepo konstrutora.
        /// </summary>
        public DatabaseRepo()
        {
            dB = new DatabaseEntities();
        }

        /// <summary>
        /// Név alapján visszaadja a felhasználót.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <returns>Visszaadja a felhasználót.</returns>
        public felhasznalo BejelentkezettFelhasznalo(string nev)
        {
            return this.GetAllfelhasznalo().Where(x => x.felhasznalonev == nev).First();
        }

        /// <summary>
        /// Kilistázza az összes felhasználót.
        /// </summary>
        /// <returns>A felhasználók listája.</returns>
        public List<felhasznalo> GetAllfelhasznalo()
        {
            return dB.felhasznalo.ToList();
        }

        /// <summary>
        /// Kilistázza az összes könyvet.
        /// </summary>
        /// <returns>A könyvek listája.</returns>
        public List<konyvek> GetAllkonyvek()
        {
            return dB.konyvek.ToList();
        }

        /// <summary>
        /// Kilistázza az összes publikációt.
        /// </summary>
        /// <returns>A publikációk listája.</returns>
        public List<publikal> GetAllpublikal()
        {
            return dB.publikal.ToList();
        }

        /// <summary>
        /// Kilistázza az összes vásárlást.
        /// </summary>
        /// <returns>A vásárlások listája.</returns>
        public List<vasarol> GetAllvasarol()
        {
            return dB.vasarol.ToList();
        }

        /// <summary>
        /// Megnézi, hogy betud-e jelentkezni a felhasználó.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="pw">A felhasználó jelszó paramétere.</param>
        /// <returns>Igaz, ha betud jelentkezni.</returns>
        public bool BetuDjelentkezni(string name, string pw)
        {
            var r = this.GetAllfelhasznalo();

            // foreach (var felhasznalo in r)
            // {
            //    if (felhasznalo.felhasznalonev == name && felhasznalo.jelszo == pw)
            //    {
            //        return true;
            //    }
            //    else return false;
            // }
            // return false;
            var s = dB.felhasznalo.Where(x => x.felhasznalonev == name).FirstOrDefault();
            if (s == null)
            {
                return false;

                // nincs ilyen nevű felhasználó
            }
            else
            {
                if (s.jelszo == this.Sha256Encrypt(pw))
                {
                    return true;
                }
                else
                {
                    return false;

                    // nemjó jelszó
                }
            }
        }

        /// <summary>
        /// A megadott felhasználót az adatbázishoz adja.
        /// </summary>
        /// <param name="felh">Maga a felhasználó.</param>
        public void AdatbazishozHozzaAd(felhasznalo felh)
        {
            felh.jelszo = this.Sha256Encrypt(felh.jelszo);
            dB.felhasznalo.Add(felh);
            dB.SaveChanges();
        }

        /// <summary>
        /// A megadott felhasználót az adatbázisból eltávolítja.
        /// </summary>
        /// <param name="felh">Maga a felhasználó.</param>
        public void AdatbazisbolEltavoilit(felhasznalo felh)
        {
            var db = dB.felhasznalo.Where(u => u.felhasznalonev.Equals(felh.felhasznalonev.ToString())).FirstOrDefault();

            dB.felhasznalo.Remove(db);
            dB.SaveChanges();
        }

        /// <summary>
        /// A megadott könyvet az adatbázishoz adja.
        /// </summary>
        /// <param name="konyv">Maga a könyv.</param>
        public void KonyvAdatbazishozHozzaAd(konyvek konyv)
        {
            dB.konyvek.Add(konyv);
            dB.SaveChanges();
        }

        /// <summary>
        /// A megadott könyvet az adatbázisból eltávolítja.
        /// </summary>
        /// <param name="konyv">Maga a könyv.</param>
        public void KonyvAdatbazishozbolEltavolit(konyvek konyv)
        {
            var db = dB.konyvek.Where(u => u.ISBN.Equals(konyv.ISBN.ToString())).FirstOrDefault();

            dB.konyvek.Remove(db);
            dB.SaveChanges();
        }

        /// <summary>
        /// A megadott vásárlást az adatbázishoz adja.
        /// </summary>
        /// <param name="vasarlas">Maga a vásárlás.</param>
        public void VasarlasAdatbazishozHozzaAd(vasarol vasarlas)
        {
            dB.vasarol.Add(vasarlas);
            dB.SaveChanges();
        }

        /// <summary>
        /// A megadott vásárlást az adatbázisból eltávolítja.
        /// </summary>
        /// <param name="testnev">Maga a felhasználónév.</param>
        public void VasarlasAdatbazisbolEltavolit(string testnev)
        {
            var db = dB.vasarol.Where(u => u.felhasznalo.felhasznalonev.Equals(testnev)).FirstOrDefault();

            dB.vasarol.Remove(db);
            dB.SaveChanges();
        }

        /// <summary>
        /// Megnézi, hogy van-e ilyen felhasználó.
        /// </summary>
        /// <param name="name">A felhasználó neve.</param>
        /// <returns>Igaz, ha van.</returns>
        public bool VanEIlyenFElhasznalo(string name)
        {
            bool vissza = false;
            var felhasznalok = this.GetAllfelhasznalo();
            foreach (var felhasznalo in felhasznalok)
            {
                if (felhasznalo.felhasznalonev == name)
                {
                    vissza = true;
                }
            }

            return vissza;

            // if (DB.felhasznalo.Where(x => x.felhasznalonev == name).First() != null)
            // {
            //    return true; // van ilyen
            // }
            // else
            // {
            //    return false; //nincs ilyen
            // }
        }

        /// <summary>
        /// Megnézi, hogy van-e ilyen könyv.
        /// </summary>
        /// <param name="isbn">A könyv isbn-je.</param>
        /// <returns>Igaz, ha van.</returns>
        public bool VanEIlyenKonyv(string isbn)
        {
            bool vissza = false;
            var konyvek = this.GetAllkonyvek();
            foreach (var konyv in konyvek)
            {
                if (konyv.ISBN == isbn)
                {
                    vissza = true;
                }
            }

            return vissza;
        }

        /// <summary>
        /// Hozzáadja a könyvet az adabázishoz.
        /// </summary>
        /// <param name="konyv">Maga a könyv.</param>
        public void Elfogad(konyvek konyv)
        {
            dB.konyvek.Where(x => x.ISBN == konyv.ISBN).First().engedelyezvee = 1;
            dB.SaveChanges();
        }

        /// <summary>
        /// Eltávolítja a könyvet az adabázisból.
        /// </summary>
        /// <param name="konyv">Maga a könyv.</param>
        public void Elutasit(konyvek konyv)
        {
            dB.konyvek.Where(x => x.ISBN == konyv.ISBN).First().engedelyezvee = 0;
            dB.SaveChanges();
        }

        /// <summary>
        /// Megnézi, hogy a felhasználó létezik-e.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <returns>Ha létezik, akkor igaz.</returns>
        public bool VaneilyenAdminnaTeendo(string name, string email)
        {
            bool vissza = false;
            var felhasznalok = this.GetAllfelhasznalo();
            foreach (var felhasznalo in felhasznalok)
            {
                if (felhasznalo.nev == name && felhasznalo.email == email)
                {
                    vissza = true;
                }
            }

            return vissza;
        }

        /// <summary>
        /// Megnézi, hogy a felhasználó admin-e.
        /// </summary>
        /// <param name="name">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználó email paramétere.</param>
        /// <returns>Ha admin már, akkor igaz.</returns>
        public bool AdminEmar(string name, string email)
        {
            bool vissza = false;
            var felhasznalok = this.GetAllfelhasznalo();
            foreach (var felhasznalo in felhasznalok)
            {
                if (felhasznalo.nev == name && felhasznalo.email == email)
                {
                    if (felhasznalo.adminisztratore == 1)
                    {
                        vissza = true;
                    }
                }
            }

            return vissza;
        }

        /// <summary>
        /// A felhasználót adminná teszi.
        /// </summary>
        /// <param name="nev">A felhasználó név paramétere.</param>
        /// <param name="email">A felhasználü email paramétere.</param>
        public void AdminElfogad(string nev, string email)
        {
            dB.felhasznalo.Where(x => x.email == email && x.nev == nev).First().adminisztratore = 1;
            dB.SaveChanges();
        }

        /// <summary>
        /// A publikálást az adatbázishoz adja.
        /// </summary>
        /// <param name="ujpublikalas">Maga a publikálás.</param>
        public void FeltoltesAdatbazishozHozzaad(publikal ujpublikalas)
        {
            dB.publikal.Add(ujpublikalas);
            dB.SaveChanges();
        }

        /// <summary>
        /// A jelszó titkosítása.
        /// </summary>
        /// <param name="jelszo">Maga a jelszó.</param>
        /// <returns>A titkosított jelszó.</returns>
        public string Sha256Encrypt(string jelszo)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(jelszo);
            SHA256Managed hash = new SHA256Managed();
            byte[] hasheltByteTomb = hash.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hasheltByteTomb)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        /// <summary>
        /// Elveszi a felhasználó kuponját.
        /// </summary>
        /// <param name="felhnev">A felhasználó felhasználóneve.</param>
        public void KupontElvesz(string felhnev)
        {
            dB.felhasznalo.Where(x => x.felhasznalonev == felhnev).First().kupon = 0;
            dB.SaveChanges();
        }

        /// <summary>
        /// Kupont ad a felhasználónak.
        /// </summary>
        /// <param name="felhnev">A felhasználó felhasználónév paramétere.</param>
        /// <param name="kupon">A felhasználó kupon paramétere.</param>
        public void KupontHozzaAd(string felhnev, int kupon)
        {
            dB.felhasznalo.Where(x => x.felhasznalonev == felhnev).First().kupon = kupon;
            dB.SaveChanges();
        } 

        /// <summary>
        /// Megnézi, hogy a felhasználónak van-e kuponja.
        /// </summary>
        /// <param name="felhnev">A felhasználó felhasználónév paramétere.</param>
        /// <returns>Igaz, ha van kuponja.</returns>
        public bool VanKuponja(string felhnev)
        {
            if (dB.felhasznalo.Where(x => x.felhasznalonev == felhnev).First().kupon >0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Visszaadja a felhasználó vásárlásait.
        /// </summary>
        /// <param name="fh">A felhasználó felhasználóneve.</param>
        /// <returns>Visszaadja a vásárlásait.</returns>
        public List<vasarol> Vasarlasok(string fh)
        {
            var nev = GetAllfelhasznalo().Where(x => x.felhasznalonev == fh).First().nev;
            var email = GetAllfelhasznalo().Where(x => x.felhasznalonev == fh).First().email;
            List<vasarol> vasarlasok = GetAllvasarol().Where(x => x.nev == nev && x.email == email).ToList();
            return vasarlasok;
        }

        /// <summary>
        /// Visszaadja a felhasználó publikálásait.
        /// </summary>
        /// <param name="fh">A felhasználó felhasználóneve.</param>
        /// <returns>Visszaadja a publikálásait.</returns>
        public List<publikal> Publikalasok(string fh)
        {
            var nev = GetAllfelhasznalo().Where(x => x.felhasznalonev == fh).First().nev;
            var email = GetAllfelhasznalo().Where(x => x.felhasznalonev == fh).First().email;
            List<publikal> publikalasok = GetAllpublikal().Where(x => x.nev == nev && x.email == email).ToList();
            return publikalasok;
        }


    }
}
