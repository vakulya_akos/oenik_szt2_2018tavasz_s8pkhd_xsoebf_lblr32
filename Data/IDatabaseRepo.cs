﻿// <copyright file="IDatabaseRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A databaserepo interface-e.
    /// </summary>
    public interface IDatabaseRepo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<felhasznalo> GetAllfelhasznalo();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<vasarol> GetAllvasarol();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<publikal> GetAllpublikal();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<konyvek> GetAllkonyvek();
    }
}

