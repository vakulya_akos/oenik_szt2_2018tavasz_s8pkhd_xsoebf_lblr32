﻿// <copyright file="VMTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using GUI;
    using Logic;
    using NUnit.Framework;

    /// <summary>
    /// A VM teszt osztálya.
    /// </summary>
    [TestFixture]
    public class VMTest
    {
        /// <summary>
        /// Böngészés VM kosárba helyezés tesztje
        /// </summary>
        [Test]
        public void BongeszesKosarbaHelyezesTeszt()
        {
            WeBestSellerBL.Felhasznalo = "raj999";
            konyvek tesztkonyv = new konyvek() { ar = 2000, mufaj = "szakirodalom", szerzo = "Dr. Lennart Hofstatter", ISBN = "111-111-111-2223", cim = "Hurelmelet", ev = 2001, engedelyezvee = 1 };
            BongeszesViewModel bmv = new BongeszesViewModel();
            bmv.MegvasaralhatoKonyvek.Add(tesztkonyv);
            bmv.Kijelolt_konyv = tesztkonyv;
            bmv.KosarbaHelyezes();

            Assert.That(bmv.KosarbanlevoKonyvek.Last().ISBN, Is.EqualTo(tesztkonyv.ISBN));
        }

        /// <summary>
        /// Kosárba helyezett könyvek már nicnsenek a Megvásárolható könyvek közöttt
        /// </summary>
        [Test]
        public void KosarbaHelyezesUtanNemTartalmazzaAMegvasarolhatoKonyvek()
        {
            WeBestSellerBL.Felhasznalo = "raj999";
            konyvek tesztkonyv = new konyvek() { ar = 2000, mufaj = "szakirodalom", szerzo = "Dr. Lennart Hofstatter", ISBN = "111-111-111-2223", cim = "Hurelmelet", ev = 2001, engedelyezvee = 1 };
            BongeszesViewModel bmv = new BongeszesViewModel();
            bmv.MegvasaralhatoKonyvek.Add(tesztkonyv);
            bmv.Kijelolt_konyv = tesztkonyv;
            bmv.KosarbaHelyezes();

            Assert.That(bmv.MegvasaralhatoKonyvek, Is.Not.Contains(tesztkonyv));
        }

        /// <summary>
        /// Admin könyv engedélyezés/Elutasítás után a könyv már nem Elbírálatlaan könyv
        /// </summary>
        [Test]
        public void AdminisztralasEngedelyezesreVaroKonyvekbolEltunikAzElutasitottKonyv()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            WeBestSellerBL.Felhasznalo = "she999";
            konyvek tesztkonyv = new konyvek() { ISBN = "tesztISBN55", cim = "tesztcim6", mufaj = "tesztmufaj6", szerzo = "tesztszerzo6", ar = 1000, ev = 2010, engedelyezvee = 0 };
            dbr.KonyvAdatbazishozbolEltavolit(tesztkonyv);
            WeBestSellerBL.AdatbazishozHozzaAdKonyv(tesztkonyv);

            AdminisztralasViewModel vm = new AdminisztralasViewModel();
            vm.ElbiralatlanKonyvek.Add(tesztkonyv);
            vm.KijeloltKonyv = tesztkonyv;
            vm.Elutasit(vm.KijeloltKonyv);

            var res = vm.ElbiralatlanKonyvek;
            Assert.IsFalse(res.Contains(tesztkonyv));
        }

        /// <summary>
        /// Böngészés VM keresés találat megfelelő megjelenítése
        /// </summary>
        [Test]
        public void BongeszesKeresesTalalatMegjelenitese()
        {
            WeBestSellerBL.Felhasznalo = "raj999";
            BongeszesViewModel vm = new BongeszesViewModel();
            vm.Keres("trónok");
            Assert.That(vm.KeresettKonyvek.Count, Is.EqualTo(1));
        }
    }
}
