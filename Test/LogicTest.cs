﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using GUI;
    using Logic;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// A logika teszt osztálya.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        /// <summary>
        /// Test source a kupon teszteléséhez
        /// </summary>
        private static object[] kuponArOsszegzesSource =
        {
            new object[] { 100, 10, 90 },
            new object[] { 600, 25, 450 },
            new object[] { 500, 50, 250 }
        };

        /// <summary>
        /// Egyszeri beállítás.
        /// </summary>
        [OneTimeSetUp]
        public void Init()
        {
            WeBestSellerBL wb = new WeBestSellerBL();
            felhasznalo tesztfelhasznalo = new felhasznalo() { nev = "tesztnev", adminisztratore = 0, email = "tesztemail", felhasznalonev = "tesztfelhaznalo", jelszo = "tesztjelszo", kupon = 123 };
            felhasznalo tesztfelhasznalo2 = new felhasznalo() { nev = "tesztnev2", adminisztratore = 0, email = "tesztemail2", felhasznalonev = "tesztfelhaznalo2", jelszo = "tesztjelszo2", kupon = 123 };
            felhasznalo tesztfelhasznalo3 = new felhasznalo() { nev = "tesztnev3", adminisztratore = 0, email = "tesztemail3", felhasznalonev = "tesztfelhaznalo3", jelszo = "tesztjelszo3", kupon = 123 };
        }

        /// <summary>
        /// A bejelentkezés működésének tesztelése
        /// </summary>
        /// <param name="felhasznalonev">A tesztelendő felhasználó név paramétere.</param>
        /// <param name="jelszo">A tesztelendő felhasználó jelszo paramétere.</param>
        [TestCase("raj999", "asd000")]
        public void SikeresBejelentkezesTeszt(string felhasznalonev, string jelszo)
        {
            var er = WeBestSellerBL.BetudJelentkezni(felhasznalonev, jelszo);
            Assert.IsTrue(er);
        }

        /// <summary>
        /// A bejelntkezés siekrtelen, ha nem megfelelőek az adatok.
        /// </summary>
        /// <param name="felhasznalonev">A tesztelendő felhasználó név paramétere.</param>
        /// <param name="jelszo">A tesztelendő felhasználó jelszo paramétere.</param>
        [TestCase("raj999", "asd245")]
        public void SikertelenBejelentkezesTeszt(string felhasznalonev, string jelszo)
        {
            var eredmeny = WeBestSellerBL.BetudJelentkezni(felhasznalonev, jelszo);
            Assert.IsFalse(eredmeny);
        }

        /// <summary>
        /// FelhasznaloVanE működésének tesztje
        /// </summary>
        [Test]
        public void VanEIlyenFelhasznaloTrueTeszt()
        {
            bool van = WeBestSellerBL.FElhasznaloVanE("raj999");
            Assert.IsTrue(van);
        }

        /// <summary>
        /// FelhasznaloVanE működésének false tesztje
        /// </summary>
        [Test]
        public void VanEIlyenFelhasznaloFalseTeszt()
        {
            bool van = false;
            van = WeBestSellerBL.FElhasznaloVanE("nincsilyen");
            Assert.IsFalse(van);
        }

        /// <summary>
        /// KonyvVanE működésének false tesztje
        /// </summary>
        [Test]
        public void VanEIlyenKonyvFalseTeszt()
        {
            bool van = false;
            van = WeBestSellerBL.KonyvVanE("nincsilyen");
            Assert.IsFalse(van);
        }

        /// <summary>
        /// KonyvVanE működésének tesztje új feltöltött könyv esetén
        /// </summary>
        [Test]
        public void SikeresFeltoltesTeszt()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            konyvek tesztkonyv = new konyvek() { ISBN = "tesztISBN11", cim = "tesztcim11", mufaj = "tesztmufaj8", szerzo = "tesztszerzo8", ar = 1000, ev = 2010, engedelyezvee = 0 };
            dbr.KonyvAdatbazishozbolEltavolit(tesztkonyv);
            WeBestSellerBL.AdatbazishozHozzaAdKonyv(tesztkonyv);
            bool van = WeBestSellerBL.KonyvVanE(tesztkonyv.ISBN);
            Assert.IsTrue(van);
        }

        /// <summary>
        /// KonyvVanE működésének tesztje
        /// </summary>
        [Test]
        public void VanEIlyenKonyvTrueTeszt()
        {
            bool van = false;
            van = WeBestSellerBL.KonyvVanE("111-111-111-1111");
            Assert.IsTrue(van);
        }

        /// <summary>
        /// Regisztráció működésének tesztje tesztfelhasználóval
        /// </summary>
        [Test]
        public void SikeresRegisztracioTeszt()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            WeBestSellerBL bl = new WeBestSellerBL();
            felhasznalo tesztfelhasznalo2 = new felhasznalo() { nev = "tesztnev7", adminisztratore = 0, email = "tesztemail7", felhasznalonev = "tesztfelhaznalo7", jelszo = "tesztjelszo7", kupon = 123 };
            dbr.AdatbazisbolEltavoilit(tesztfelhasznalo2);
            WeBestSellerBL.AdatbazishozHozzaAd(tesztfelhasznalo2);
            bool van = WeBestSellerBL.FElhasznaloVanE(tesztfelhasznalo2.felhasznalonev.ToString());
            Assert.IsTrue(van);
        }

        /// <summary>
        /// Admin Konyv elfogadásának tesztje új tesztkönyv hozzáadásával
        /// </summary>
        [Test]
        public void AdminKonyvElfogadasTest()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            AdminisztratotFelulet bl = new AdminisztratotFelulet();

            konyvek tesztkonyv = new konyvek() { ISBN = "tesztISBN6", cim = "tesztcim6", mufaj = "tesztmufaj6", szerzo = "tesztszerzo6", ar = 1000, ev = 2010, engedelyezvee = 0 };
            dbr.KonyvAdatbazishozbolEltavolit(tesztkonyv);
            WeBestSellerBL.AdatbazishozHozzaAdKonyv(tesztkonyv);
            bl.KonyvElfogadas(tesztkonyv);
            Assert.That(tesztkonyv.engedelyezvee, Is.EqualTo(1));
        }

        /// <summary>
        /// Admin E az aktuális felhasználó ellenőrzésének tesztje false
        /// </summary>
        [Test]
        public void AdminEAzAktualisFelhasznaloFalseTeszt()
        {
            WeBestSellerBL bl = new WeBestSellerBL();
            WeBestSellerBL.Felhasznalo = "raj999";
            Assert.IsFalse(WeBestSellerBL.AdminE());
        }

        /// <summary>
        /// Admin E az aktuális felhasználó ellenőrzésének tesztje true
        /// </summary>
        [Test]
        public void AdminEAzAktualisFelhasznaloTrueTeszt()
        {
            WeBestSellerBL bl = new WeBestSellerBL();
            WeBestSellerBL.Felhasznalo = "she999";
            Assert.IsTrue(WeBestSellerBL.AdminE());
        }

        /// <summary>
        /// Uj tesztfelhasználó adminná tételének tesztje
        /// </summary>
        [Test]
        public void AdminnaTesztTeszt()
        {
            WeBestSellerBL bl = new WeBestSellerBL();
            DatabaseRepo dbr = new DatabaseRepo();
            felhasznalo tesztfelhasznalo3 = new felhasznalo() { nev = "tesztnev3", adminisztratore = 0, email = "tesztemail99", felhasznalonev = "tesztfelhaznalo99", jelszo = "tesztjelszo3", kupon = 123 };
            dbr.AdatbazisbolEltavoilit(tesztfelhasznalo3);
            WeBestSellerBL.AdatbazishozHozzaAd(tesztfelhasznalo3);
            bool admin = WeBestSellerBL.AdminnaTesz(tesztfelhasznalo3.nev, tesztfelhasznalo3.email);
            Assert.That(tesztfelhasznalo3.adminisztratore, Is.EqualTo(1));
        }

        /// <summary>
        /// Kosárban lévő könyvek árának összegzésének tesztje
        /// </summary>
        [Test]
        public void ArOsszegzesTeszt()
        {
            konyvek tesztkonyv1 = new konyvek() { ISBN = "tesztISBN6", cim = "tesztcim6", mufaj = "tesztmufaj6", szerzo = "tesztszerzo6", ar = 1000, ev = 2010, engedelyezvee = 0 };
            konyvek tesztkonyv2 = new konyvek() { ISBN = "tesztISBN7", cim = "tesztcim7", mufaj = "tesztmufaj7", szerzo = "tesztszerzo7", ar = 1, ev = 2010, engedelyezvee = 0 };
            konyvek tesztkonyv3 = new konyvek() { ISBN = "tesztISBN8", cim = "tesztcim8", mufaj = "tesztmufaj8", szerzo = "tesztszerzo8", ar = 666, ev = 2010, engedelyezvee = 0 };
            List<konyvek> testkonyvek = new List<konyvek>();
            testkonyvek.Add(tesztkonyv1);
            testkonyvek.Add(tesztkonyv2);
            testkonyvek.Add(tesztkonyv3);

            var result = WeBestSellerBL.ArOsszegzes(testkonyvek);

            Assert.That(result, Is.EqualTo(1667));
        }

        /// <summary>
        /// Kuponos ár kiszámításának tesztje TestCaseSource felhasználásával
        /// </summary>
        /// <param name="a">Eredeti ár</param>
        /// <param name="b">Kupon értéke.</param>
        /// <param name="c">Kuponnal csökkentett ár.</param>
        [Test]
        [TestCaseSource("kuponArOsszegzesSource")]
        public void KuponArOsszegzesTeszt(int a, int b, int c)
        {
            DatabaseRepo dbr = new DatabaseRepo();
            felhasznalo tesztfelhasznalokupon = new felhasznalo() { nev = "tesztnevkupon", adminisztratore = 0, email = "tesztemailkupon", felhasznalonev = "tesztfelhaznalokupon", jelszo = "tesztjelszokupon", kupon = b };
            dbr.AdatbazisbolEltavoilit(tesztfelhasznalokupon);
            WeBestSellerBL.AdatbazishozHozzaAd(tesztfelhasznalokupon);
            WeBestSellerBL.Felhasznalo = tesztfelhasznalokupon.felhasznalonev;
            var res = WeBestSellerBL.KuponArOsszegzes(a);
            Assert.That(res, Is.EqualTo(c));
        }

        /// <summary>
        /// Nem vásárolhat kupont ha már van neki
        /// </summary>
        [Test]
        public void KupontVasarolFalseMertVanTeszt()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            felhasznalo tesztfelhasznalokupon = new felhasznalo() { nev = "tesztnevkupon", adminisztratore = 0, email = "tesztemailkupon", felhasznalonev = "tesztfelhaznalokupon", jelszo = "tesztjelszokupon", kupon = 11 };
            dbr.AdatbazisbolEltavoilit(tesztfelhasznalokupon);
            WeBestSellerBL.AdatbazishozHozzaAd(tesztfelhasznalokupon);
            WeBestSellerBL.Felhasznalo = tesztfelhasznalokupon.felhasznalonev;
            var res = WeBestSellerBL.KupontVasarol();
            Assert.IsFalse(res);
        }

        /// <summary>
        /// Sikeresen kupont vásárol mert még nincs neki
        /// </summary>
        [Test]
        public void KupontVasarolSikeresenTeszt()
        {
            DatabaseRepo dbr = new DatabaseRepo();
            felhasznalo tesztfelhasznalokupon = new felhasznalo() { nev = "tesztnevkupon", adminisztratore = 0, email = "tesztemailkupon", felhasznalonev = "tesztfelhaznalokupon", jelszo = "tesztjelszokupon", kupon = 0 };
            dbr.AdatbazisbolEltavoilit(tesztfelhasznalokupon);
            WeBestSellerBL.AdatbazishozHozzaAd(tesztfelhasznalokupon);
            WeBestSellerBL.Felhasznalo = tesztfelhasznalokupon.felhasznalonev;
            var res = WeBestSellerBL.KupontVasarol();
            Assert.That(tesztfelhasznalokupon.kupon, Is.GreaterThan(0));
        }

        /// <summary>
        /// Mocked Repo tesztje
        /// </summary>
        [Test]
        public void MockedRepositoryKonyvekTest()
        {
            // ARRANGE
            Mock<IDatabaseRepo> testRepo = this.CreateMockedRepository();
            Assert.That(testRepo.Object.GetAllkonyvek().Count, Is.EqualTo(2));
        }

        private Mock<IDatabaseRepo> CreateMockedRepository()
        {
            Mock<IDatabaseRepo> testKonyvRepositoryMock = new Mock<IDatabaseRepo>();

            List<konyvek> testKonyvek = new List<konyvek>();
            testKonyvek.Add(new konyvek()
            {
                cim = "testkonyv1",
                ar = 1111,
                szerzo = "testszerzo1",
                ISBN = "testisbn1",
                ev = 2010,
                mufaj = "testmufaj1",
                engedelyezvee = 1,
                Cim = "testkonyv1"
            });
            testKonyvek.Add(new konyvek()
            {
                cim = "testkonyv2",
                ar = 1111,
                szerzo = "testszerzo2",
                ISBN = "testisbn2",
                ev = 2010,
                mufaj = "testmufaj2",
                engedelyezvee = 1,
                Cim = "testkonyv2"
            });

            testKonyvRepositoryMock.Setup(m => m.GetAllkonyvek()).Returns(testKonyvek.ToList());

            return testKonyvRepositoryMock;
        }
    }
}
